from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient
# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'id',
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        'recipe_title',
        'order',
        'id',
    )

@admin.register(Ingredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = (
        'amount',
        'food_item',
        'recipe'
    )
